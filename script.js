window.addEventListener('scroll', function() {
    var header = document.querySelector('header');
    var section = document.querySelector('section');
    var scrollPosition = window.scrollY;
    
    if (scrollPosition > 0) {
      header.classList.add('header-shadow');
    } else {
      header.classList.remove('header-shadow');
    }
  });
  